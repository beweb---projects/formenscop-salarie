<?php 
namespace BWB\Framework\mvc;
use BWB\Framework\mvc\CRUDInterface;
use BWB\Framework\mvc\RepositoryInterface;
use PDO;

/**
 * Cette classe sert à gérer l'accées aux données via l'utilisation d'objets PDO
 */
abstract class DAO implements CRUDInterface, RepositoryInterface {
    /**
     * Cette propriété permet de stocker touts nos objets PDO au même endroit
     */
    protected $pdo;

    /**
     * On récupère les données entrées dans le fichier config.json que l'on store dans une variable,
     * on initialise une variable $dsn qui correspond à la config de notre DAO pour accéder à notre base de données.
     * On attribut ensuite new PDO() à la propriété $pdo du DAO
     */
    public function __construct(){
        $config = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/config/database.json"),true);
        $dsn = $config['driver'].":dbname=".$config['dbname'].";host=".$config['host'].";charset=".$config['charset'];
        $newPdo = new PDO($dsn, $config['username'],$config['password']);
        // $newPdo = new PDO("mysql:dbname=DB_blackbooks;host=80;charset=UTF8", "lapeyre", "root");
        $this->pdo = $newPdo;
    }

}