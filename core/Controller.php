<?php
namespace BWB\Framework\mvc;

abstract class Controller
{
    /**
     * Propriétées initialisées à l'instanciation d'un controller concret, elles sont égales aux
     * super globales de même nom
     */
    private $get;
    private $post;

    /**
     * Le constructeur initialise les valeurs des propriétés get et post avec les valeurs des superglobales correspondantes
     * à l'instanciation d'un nouveau controller
     */
    public function __construct(){
        $this->get = $_GET;
        $this->post = $_POST;
    }

    /**
     * Cette méthode permet d'afficher la vue demandée, le premier argument correspond au chemin du fichier, 
     * le deuxième les données à injecter
     * Si datas n'est pas indiqué render() affichera une vue statique
     * @* @param array $datas tableau associatif dont la clé crrespon aux variables utilisées dans la vue
     */
    final protected function render($path, $datas=null){
        if(!is_null($datas)){
            foreach($datas as $key=>$value){
                $$key = $value;
            }
        }
        include $_SERVER['DOCUMENT_ROOT']."/views/".$path.".php";
    }

    /**
     * Retourne la valeur de la propriété get pour que l'utilisateur puisse la consulter
     */ 
    protected function inputGet()
    {
        return $this->get;
    }

    /**
     * Retourne la valeur de la propriété post pour que l'utilisateur puisse la consulter
     */ 
    protected function inputPost()
    {
        return $this->post;
    }
}