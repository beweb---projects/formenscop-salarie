<?php
namespace BWB\Framework\mvc\generator;

abstract class DAOGenerator
{
    private static $nom;

    /**
     * J'ouvre le fichier au chemin indiqué, si il n'existe pas le créé et place le curseur au début du fichier.
     * Ensuite j'écris ligne par ligne 
     */
    private function writeFile()
    {
        $fileName = ucfirst(DAOGenerator::$nom) . "DAO.php";
        $path = $_SERVER['DOCUMENT_ROOT']."dao/".$fileName; // Chemin du fichier à créer 
        $handle = fopen($path, 'c');
        fwrite($handle,"<?php \n");
        fwrite($handle,"namespace BWB\Framework\mvc\dao; \n");
        fwrite($handle,"use BWB\Framework\mvc\DAO; \n");
        fwrite($handle,"use BWB\Framework\mvc\models\\".ucfirst(DAOGenerator::$nom)."; \n");
        fwrite($handle,"use PDO; \n");
        fwrite($handle,"class ".ucfirst(DAOGenerator::$nom)."DAO extends DAO { \n \n");
            fwrite($handle, DAOGenerator::createRetrieve());
            fwrite($handle, DAOGenerator::createUpdate());
            fwrite($handle, DAOGenerator::createDelete());
            fwrite($handle, DAOGenerator::createCreate());
            fwrite($handle, DAOGenerator::createGetAll());
            fwrite($handle, DAOGenerator::createGetAllBy());
        fwrite($handle,"}");   
        fclose($handle);
    }

    /**
     * Méthode de création de la méthode retrieve 
     */
    private function createRetrieve()
    {
        $method = "/** \n".
        "* Methodes de CRUDInterfacce \n".
        "*/ \n".
        "public function retrieve(int ".'$id'.") : Object \n".
        "{ \n".
            '   $db = $this->pdo;'."\n".
            '   $statement = $db->query("select * from '.DAOGenerator::$nom.' where id=".$id);'."\n".
            '   $statement->setFetchMode(PDO::FETCH_CLASS, "BWB\\Framework\\mvc\\models"."\\'.ucfirst(DAOGenerator::$nom).'" );'."\n".
            '   $row = $statement->fetch();'."\n \n".
            '   return $row;'."\n".
        "} \n \n";
        
        return $method; 
    }

    /**
     * Méthode de création de la méthode update
     */
    private function createUpdate()
    {
        $method = "public function update(array ".'$datas'.") : bool \n".
        "{ \n".
            '   $db = $this->pdo;'."\n".
            '   $query = "update '.DAOGenerator::$nom.' set ";'."\n".
            '   foreach($datas as $key=>$value){'."\n".
                '       if($key == "id"){'."\n".
                    '           $id = $value;'."\n".
                '       }else{'."\n".
                    '           $query = $query.$key." = \'".$value."\',";'."\n".
                '       }'."\n".
            '   }'."\n".
            '   $query = substr_replace($query, " ", -1, 1);'."\n".
            '   $query = $query."where id=".$id;'."\n \n".
            '   $statement = $db->query($query);'."\n".
            '   $count = $statement->execute();'."\n \n".
            '   return boolval($count);'."\n".
        "} \n \n";
        
        return $method;  
    }

    /**
     * Méthode de création de la méthode delete
     */
    private function createDelete()
    {
        $method = "public function delete(int ".'$id'.") : bool \n".
        "{ \n".
            '   $db = $this->pdo;'."\n".
            '   $count = $db->exec("delete from '.DAOGenerator::$nom.' where id=".$id);'."\n \n".
            '   return boolval($count);'."\n".
        "} \n \n";
        
        return $method; 
    }

    /**
     * Méthode de création de la méthode create
     */
    private function createCreate()
    {
        $method = "public function create(array ".'$data'.") : Object \n".
        "{ \n".
            '   $db = $this->pdo;'."\n".
            '   $query = "insert into '.DAOGenerator::$nom.' (";'."\n".
            '   foreach($data as $key=>$value){'."\n".
                '       $query = $query.$key.",";'."\n".
            '   }'."\n".
            '   $query = substr_replace($query, ")", -1, 1);'."\n".
            '   $query = $query." values (";'."\n".
            '   foreach($data as $key=>$value){'."\n".
                '       $query = $query."\'".$value."\',";'."\n".
            '   }'."\n".
            '   $query = substr_replace($query, ")", -1, 1);'."\n".
            '   $query = $query.";";'."\n \n".
            '   $statement = $db->exec($query);'."\n".
            '   $lastId = $db->lastInsertId();'."\n \n".
            '   return $this->retrieve($lastId);'."\n".
        "} \n \n";
        
        return $method; 
    }

    /**
     * Méthode de création de la méthode getAll
     */
    private function createGetAll()
    {
        $method = "/** \n".
        "* Methodes de RepositoryInterface \n".
        "*/  \n \n".
        "public function getAll() : Object \n".
        "{ \n".
            '   $db = $this->pdo;'."\n".
            '   $statement = $db->query("select * from '.DAOGenerator::$nom.' ");'."\n".
            '   $statement->setFetchMode(PDO::FETCH_CLASS, "BWB\\Framework\\mvc\\models"."\\'.ucfirst(DAOGenerator::$nom).'");'."\n".
            '   $liste = $statement->fetchAll();'."\n \n".
            '   return $liste;'."\n".
            "} \n \n";
        
        return $method;
    }

    /**
     * Méthode de création de la méthode getAllBy
     */
    private function createGetAllBy()
    {
        $method = "public function getAllBy(array ".'$filters'.") : Object \n".
        "{ \n".
            '   $db = $this->pdo;'."\n \n".
            '   $query = "select * from '.DAOGenerator::$nom.' where ";'."\n".
            '   foreach($filters as $key=>$value){'."\n".
                '       $query = $query.$key." = ".$value." and ";'."\n".
            '   }'."\n".
            '   $query = substr_replace($query, ";", -5, 5);'."\n \n".
            '   $statement = $db->query($query);'."\n".
            '   $statement->setFetchMode(PDO::FETCH_CLASS, "BWB\\Framework\\mvc\\models"."\\'.ucfirst(DAOGenerator::$nom).'");'."\n".
            '   $liste = $statement->fetchAll();'."\n \n".
            '   return $liste;'."\n".
        "} \n \n";
        
        return $method; 
    }

     /**
     * Methode qui créera la Classe correspondant au paramètre passé en argument
     */
    static function generate(string $name){
        DAOGenerator::$nom = $name;
        // Appel des diverses méthodes private
        DAOGenerator::writeFile();
    }
}
