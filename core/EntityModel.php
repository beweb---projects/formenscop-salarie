<?php
namespace BWB\Framework\mvc;
use BWB\Framework\mvc\Persistable;

/**
 * La classe abstraite EntityModel permet de faciliter l'utilisation du DAO par un développeur futur.
 * Elle sera, comme son nom l'indique, un modèle parent pour les entités utilisées.
 * Elle est abstraite car il ne doit pas être possible d'instancier une EntityModel et elle implemente l'interface Persistable
 * pour utiliser les manipulations CRUD
 */
abstract class EntityModel implements Persistable
{
    /**
     * @param CRUDInterface $dao Propriété de type CRUDInterface, pour ne récupérer que les méthodes de celles ci
     */
    protected $dao;

    /**
     * Chaque entité possède un id, on a donc besoin d'une propriété correspondante
     */
    protected $id;

    /**
     * On implémente les méthodes de persistable ici, car elles seront les mêmes pour chaque entitée enfante
     */
    public function load()
    {
        $e = $this->dao->retrieve($this->id);

        // On créé un nouvel object ReflectionClass qui permet de récupérer des informations sur la classe passée en argument
        $class = new ReflectionClass($this);
        $className = $class->getName();
        // On récupère les méthodes de la classe via la méthode getMethods
        $methods = $class->getMethods();
        
        // Je boucle dans la liste de méthodes 
        for($i = 0, $size = count($methods);  $i < $size; $i++){
            if($methods[$i]->getDeclaringClass() != 'EntityModel' ){                    // Je vérifie que la méthode est celle de l'entité
                $strPrefix = substr($methods[$i]->getName(), 0, 3);                     // Je récupère les 3 premiers caractères du nom de la méthode
                if($strPrefix == 'set'){                                                // Je ne prend que les méthodes qui commencent par 'set' (donc les setters)
                    $methodNameWithPrefix = $methods[$i]->getName();
                    $methodName = str_replace('set', '', $methodNameWithPrefix);        // Je supprime le prefixe 'set'
                    $propertyName = lcfirst($methodName);                               // J'enlève la première majuscule de la chaine de caractère

                    // Je créée un nouvel object ReflectionMethod qui me permet d'utiliser la méthode invoke
                    $reflectionMethod = new ReflectionMethod($className, $methods[$i]->getName());      // Premier paramètre le nom de la class, deuxième la méthode
                    $invoke = $reflectionMethod->invoke($this, $e[$propertyName]);      // invoke me permet d'invoquer la méthode actuelle avec en parametre le deuxième argument
                }
            }            
        }
    }

    public function update(array $data)
    {
        if($this->id){
            $this->dao->update($data);
        }else{
            $this->dao->create($data);
        }
    }

    public function remove()
    {
        $this->dao->delete($this->id);
    }
}