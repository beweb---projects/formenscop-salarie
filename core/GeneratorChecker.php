<?php
namespace BWB\Framework\mvc\generator;
use PDO;

abstract class GeneratorChecker 
{
    /**
     * Méthode qui créé un objet pdo pour se connecter à la base de donnée et vérifie si le nom de la table passée
     * en argument existe, retourne true ou false
     */
    static function checkTableName(string $tableName) : bool
    {
        $config = json_decode(file_get_contents("config/database.json"),true);
        $dsn = $config['driver'].":dbname=".$config['dbname'].";host=".$config['host'].";charset=".$config['charset'];
        $db = new PDO($dsn, $config['username'],$config['password']);

        $statement = $db->query("show tables");
        while ($row = $statement->fetch(PDO::FETCH_NUM)) {
            if($row[0] == $tableName){
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode qui récupère le nom des tables de la DBB et qui les renvoie sous forme de tableau
     */
    static function getTablesNames() : array
    {
        $config = json_decode(file_get_contents("config/database.json"),true);
        $dsn = $config['driver'].":dbname=".$config['dbname'].";host=".$config['host'].";charset=".$config['charset'];
        $db = new PDO($dsn, $config['username'],$config['password']);

        $statement = $db->query("show tables");
        $tablesNames = $statement->fetchAll();

        return $tablesNames;
    }
}