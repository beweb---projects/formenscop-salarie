	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="/dashboard">
				<img src="/vendors/images/deskapp-logo.png" alt="">
			</a>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">

					<!-- liens en DropDown pour l'annuaire -->

					<li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle">
							<span class="fa fa fa-address-book-o"></span><span class="mtext">Annuaire</span>
						</a>
						<ul class="submenu">
							<li><a href="/contact/stagiaireF">Stagiaires (en formation)</a></li>
							<li><a href="/contact/stagiaireA">Stagiaires (en accompagnement)</a></li>
							<li><a href="/contact/entreprise">Entreprises</a></li>
							<li><a href="/contact/accompagnateur">Accompagnateurs</a></li>
							<!-- <li><a href="/contact/stagiaires">Stagiaires (du coordinateur)</a></li>
							<li><a href="/contact/formateur">Formateurs</a></li> -->
						</ul>
					</li>


					<!-- lien pour Planning-->
					<li>
						<a href="/planning" class="dropdown-toggle no-arrow">
							<span class="fa fa-calendar-o"></span><span class="mtext">Planning</span>
						</a>
					</li>
					
					
					<!-- lien pour messagerie -->
					<li>
						<a href="/messagerie" class="dropdown-toggle no-arrow">
							<!-- text danger est la pastille pour la notif contenant actuellement New -->
							<span class="fa fa-comments-o"></span><span class="mtext">Messagerie</span>
						</a>
					</li>

					
				</ul>
			</div>
		</div>
	</div>