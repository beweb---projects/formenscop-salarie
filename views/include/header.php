	<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="index.php">
					<img src="/vendors/images/logo.png" alt="LOGO" class="mobile-logo">
				</a>
			</div>
			<div class="menu-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
						<span class="user-name">Nom d'utilisateur</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="profil.php"><i class="fa fa-user-md" aria-hidden="true"></i> Profil</a>
						<a class="dropdown-item" href="login.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Déconnexion</a>
					</div>
				</div>
			</div>
			<div class="user-notification">
				<div class="dropdown">
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy fa fa-calendar" aria-hidden="true"></i>
						<span class="fi-burst-new text-danger new"></span>
					</a>
					<a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
						<i class="icon-copy fa fa-envelope-o" aria-hidden="true"></i>
						<span class="fi-burst-new text-danger new"></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="notification-list mx-h-350 customscroll">
							<ul>
								<li>
									<a href="#">
										<img src="/vendors/images/img.jpg" alt="">
										<h3 class="clearfix">User 1 <span>3 mins ago</span></h3>
										<p>detail de la notif</p>
									</a>
								</li>
								<li>
									<a href="#">
										<img src="/vendors/images/img.jpg" alt="">
										<h3 class="clearfix">User 2 <span>3 mins ago</span></h3>
										<p>detail de notif 2</p>
									</a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>