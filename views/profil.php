<!DOCTYPE html>
<html>
<head>
	<?php include('include/head.php'); ?>
	<link rel="stylesheet" type="text/css" href="/src/plugins/cropperjs/dist/cropper.css">
</head>
<body>
	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">
		<div class="pd-ltr-20 customscroll customscroll-10-p height-100-p xs-pd-20-10">
			<div class="min-height-200px">
				<div class="page-header">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="title">
								<h4>Profil</h4>
							</div>
							<nav aria-label="breadcrumb" role="navigation">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="home.php">Accueil</a></li>
									<li class="breadcrumb-item active" aria-current="page">Profil</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 mb-30">
						<div class="pd-20 bg-white border-radius-4 box-shadow">
							
							<h5 class="text-center">Nom Prenom</h5>
							<p class="text-center text-muted">Detail</p>
							<div class="profile-info">
								<h5 class="mb-20 weight-500">Contact Information</h5>
								<ul>
									<li>
										<span>Email :</span>
										FerdinandMChilds@test.com
									</li>
									<li>
										<span>Telephnoe:</span>
										04.67.67.67.67
									</li>
									<li>
										<span>Pays:</span>
										France
									</li>
									<li>
										<span>Addresse:</span>
										Pré des allongés<br>
										Paris, 75000
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 mb-30">
						<div class="bg-white border-radius-4 box-shadow height-100-p">
							<div class="profile-tab height-100-p">
								<div class="tab height-100-p">
									<ul class="nav nav-tabs customtab" role="tablist">
										
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#tasks" role="tab">Notes</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#setting" role="tab">Edition Profil</a>
										</li>
									</ul>
									<div class="tab-content">
										
										<!-- Setting Tab start -->
										<div class="tab-pane fade height-100-p" id="setting" role="tabpanel">
											<div class="profile-setting">
												<form>
													<ul class="profile-edit-list row">
														<li class="weight-500 col-md-12">
															<h4 class="text-blue mb-20">Menu Edition</h4>
															<div class="form-group">
																<label>Nom</label>
																<input class="form-control form-control-lg" type="text">
															</div>
															<div class="form-group">
																<label>Prenom</label>
																<input class="form-control form-control-lg" type="text">
															</div>
															<div class="form-group">
																<label>Email</label>
																<input class="form-control form-control-lg" type="email">
															</div>
															<div class="form-group">
																<label>Date of birth</label>
																<input class="form-control form-control-lg date-picker" type="text">
															</div>
															<div class="form-group">
																<label>Mot de passe </label>
																<input class="form-control form-control-lg" type="text">
															</div>
															
															<div class="form-group">
																<label>Type</label>
																<select class="selectpicker form-control form-control-lg" data-style="btn-outline-secondary btn-lg" title="Not Chosen">
																	<option>Rue</option>
																	<option>Ch.</option>
																	<option>BD.</option>
																	<option>Av.</option>
																</select>
															</div>
															<div class="form-group">
																<label>Rue</label>
																<textarea class="form-control"></textarea>
															</div>
															<div class="form-group">
																<label>Numero</label>
																<textarea class="form-control"></textarea>
															</div>
															<div class="form-group">
																<label>complement</label>
																<textarea class="form-control"></textarea>
															</div>
															<div class="form-group">
																<label>Code Postal</label>
																<textarea class="form-control"></textarea>
															</div>
															<div class="form-group">
																<label>Ville</label>
																<textarea class="form-control"></textarea>
															</div>
															<div class="form-group">
																<label>Pays</label>
																<select class="selectpicker form-control form-control-lg" data-style="btn-outline-secondary btn-lg" title="Not Chosen">
																	<option>France</option>
																	<option>Espagne</option>
																	<option>Engl.</option>
																	<option>Italie</option>
																</select>
															</div>
															
															
															<div class="form-group mb-0">
																<input type="submit" class="btn btn-primary" value="Update Information">
															</div>
														</li>
													</ul>
												</form>
											</div>
										</div>
										<!-- Setting Tab End -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('include/script.php'); ?>
	<script src="/src/plugins/cropperjs/dist/cropper.js"></script>
	<script>
		window.addEventListener('DOMContentLoaded', function () {
			var image = document.getElementById('image');
			var cropBoxData;
			var canvasData;
			var cropper;

			$('#modal').on('shown.bs.modal', function () {
				cropper = new Cropper(image, {
					autoCropArea: 0.5,
					dragMode: 'move',
					aspectRatio: 3 / 3,
					restore: false,
					guides: false,
					center: false,
					highlight: false,
					cropBoxMovable: false,
					cropBoxResizable: false,
					toggleDragModeOnDblclick: false,
					ready: function () {
						cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
					}
				});
			}).on('hidden.bs.modal', function () {
				cropBoxData = cropper.getCropBoxData();
				canvasData = cropper.getCanvasData();
				cropper.destroy();
			});
		});
	</script>
</body>
</html>