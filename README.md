Formenscop Salariés
==================
# Description


Cette application faisant parti d'un ensemble se portera sur la partie salariés 


les fonctionnionalités seront portées sur:

* Une Messagerie
* Une gestion de demande de congés
* Une gestion de planning
* Une interaction avec les ressources humaines à sa charge et les organismes externes



## Installation

Cloner le projet complet se trouvant (https://gitlab.com/beweb-lunel-04/internal-projects/formenscop-salarie) dans le Repertoire de votre ServeurWeb
* Mettre en place les Virtual Hosts si besoin.
* `Index.php` doit etre renseigné en tant que DirectoryIndex
* le "servername" doit etre renseigné en: salarié.formenscop.bwb.


mettre à jour les dépendances
en executant `composer update` dans votre termnial à la racine du projet.

il est necessaire d'installer Jqueury UI sans le DatePicker disponible [ICI](https://jqueryui.com/download)

## Mise en place

Le fichier **DataBase.json** dans le `repertoire /config`  doit etre completé afin que l'application puisse se connecter à la BDD.

Votre base de données doit contenir des utilisateurs afin de pouvoir se connecter à l'application.


## Sources

Outre Bootstastrap, Datatables, FullCalendar etc... Le projet s'articule à partir:
* D'un template consultable [ICI](https://deskapp-dashboard.herokuapp.com/) dont le Repo est [ICI](https://github.com/dropways/deskapp)
* D'un Framework sur un modele MVC développé par l'ecole Régionale du Numerique dont le repo est consultable [ICI](https://gitlab.com/beweb-lunel-04/framework-mvc-bwb)



### Auteurs
* Aurelie Dillensiger
* Gabrielle Lapeyre
* Gaetan Berchtold
